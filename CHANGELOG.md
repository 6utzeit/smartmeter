# CHANGELOG

<!--- next entry here -->

## 0.1.2
2020-12-15

### Fixes

- Abfragen in parseValues angepasst (c1569ce8332aae1815e2d4cb59980b92210f9d9c)

## 0.1.1
2020-12-14

### Fixes

- README.md hinzufügen (1788fcd86fee6269f0f2fc173ff90df4727ffab7)

## 0.1.0
2020-12-11

### Features

- Release (c3606afa4f77c9dc7d4c74075ec0b7d467e32f04)

### Fixes

- set correct board (5707fdb7add55f8e0a3915b984e875ff9169fbc7)
- Update .gitlab-ci.yml (34577b90a91c99bcada1b45d48ea071545bd3759)
- Update .gitlab-ci.yml (1ca025ee6d2e1ab408d6b98e4f27b4beb6713c69)
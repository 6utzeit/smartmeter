#include <ESP8266WiFi.h>

#include <DNSServer.h>            //Local DNS Server used for redirecting all requests to the configuration portal
#include <ESP8266WebServer.h>     //Local WebServer used to serve the configuration portal
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic WiFiManager wifiManager;
#include <InfluxDb.h>

const char* ssid = "SmartMeter";
const char* pass = "";

const char* ID = "PwrMtr1";

static uint16_t uploadInterval = 60000;
static uint16_t outputInterval = 60000;

uint32_t nextOutput = 0;
uint32_t nextUpload = 0;
uint8_t charsRead = 0;
char rxBuffer [20];
String rxID;
String mtrID = "UNKNOWN";
String mtrModel = "UNKNOWN";
String consSum = "0";
String powerL1 = "0";
String powerL2 = "0";
String powerL3 = "0";
String powerSum = "0";
String mtrState = "UNK";

#define INFLUXDB_HOST "HOSTNAME"
#define INFLUXDB_DATABASE "DB_NAME"

Influxdb influx(INFLUXDB_HOST);

void parseValues();
void uploadValues();
void outputValues();

void configModeCallback (WiFiManager *myWiFiManager) {
}

void setup() {
  Serial.begin(9600, SERIAL_7E1);
  WiFiManager wifiManager;
  Serial.println("Connecting to WiFi...");
  wifiManager.setAPCallback(configModeCallback);
  if (!wifiManager.autoConnect(ssid, pass)) {
    Serial.println("WiFI ERROR!"); 
    ESP.reset();
    delay(1000);
  }
  delay(1500);

  influx.setDb(INFLUXDB_DATABASE);
}

void parseValues() {
  if (rxID == "1-0:0.0.0*255") {
    mtrID = String(rxBuffer);
  }
  if (rxID == "1-0:1.8.0*255" || rxID == "1-0:2.8.0*255" || rxID == "1-0:15.8.0*255") {
    consSum = String(rxBuffer);
  }
  if (rxID == "1-0:21.7.0*255" || rxID == "1-0:21.7.255*255") {
    powerL1 = String(rxBuffer);
  }
  if (rxID == "1-0:41.7.0*255" || rxID == "1-0:41.7.255*255") {
    powerL2 = String(rxBuffer);
  }
  if (rxID == "1-0:61.7.0*255" || rxID == "1-0:61.7.255*255") {
    powerL3 = String(rxBuffer);
  }
  if (rxID == "1-0:1.7.0*255" || rxID == "1-0:1.7.255*255") {
    powerSum = String(rxBuffer);
  }
  if (rxID == "1-0:96.5.5*255") {
    mtrState = String(rxBuffer);
  }
  if (rxID == "0-0:96.1.255*255") {
    mtrModel = String(rxBuffer);
  }
}

void loop() {
  if (Serial.available()) {
    char c = Serial.read();
    if (c == '(') {
      rxBuffer[charsRead] = '\0';
      rxID = String(rxBuffer);
      charsRead = 0;
    } else if (c == ')') {
      rxBuffer[charsRead] = '\0';
      if (charsRead > 1 && sizeof(rxID) > 1) {
        parseValues();
      }
      charsRead = 0;
    } else if (c == 0x0D || c == 0x0A) {
      // on CR or LF
      charsRead = 0;
    } else {
      rxBuffer[charsRead++] = c;
    }
    Serial.write(c);
  }

  if (nextOutput < millis()) {
    outputValues();
    nextOutput = ( millis() + outputInterval);
  }
  if (nextUpload < millis()) {
    uploadValues();
    nextUpload = ( millis() + uploadInterval);
  }
}

void uploadValues() {
  WiFiClient client;

  InfluxData row("strom");
    row.addTag("device", "Easymeter");
    row.addTag("sensor", "Q3D");
    row.addTag("mode", "ESP8266");
    row.addValue("E", consSum.toInt());
    row.addValue("L1", powerL1.toInt());
    row.addValue("L2", powerL2.toInt());
    row.addValue("L3", powerL3.toInt());
    row.addValue("Summe", powerSum.toInt());
  influx.write(row);
}


void outputValues() {
  String output = "ID=";
  output += mtrID + ";E=" + consSum + "kW/h;L1=" + powerL1 + "W;L2=" + powerL2 + "W;L3=" + powerL3 + "W;P=" + powerSum + "W\n";
  Serial.println(output);
}
